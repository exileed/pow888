from config import TestSend, redmine, STATUS_IN_PROGRESS
from datetime import date, timedelta

yesterday = date.today() - timedelta(1)

issues = redmine.issue.filter(sort='updated_on:desc', assigned_to_id='me', status_id=STATUS_IN_PROGRESS,
                              updated_on=yesterday.strftime('%Y-%m-%d'))

send = TestSend

for issue in issues:
    if issue.status.id == STATUS_IN_PROGRESS:
        send.send_today(send, id=issue.id)
