## Отправка автоматических отчетов

###Установка
1. Копировать `.env.example` в `.env`
2. Прописать там логин пароль от redmine и от platform chat
3. Установить `pip install pipenv --user` или `python -m pip install pipenv --user`
4. Установить зависимости `pipenv install`
5. В войти в pipenv `pipenv shell`
6. Найти id чата  в который надо писать `python detectroom.py`
7. Вставить id в `.env` `GROUP_REPORT`
8. Поставить задачу в крон `crontab -e`
```bash
0 8 * * * python first.py
0 19 * * * python index.py
```

Python искать в ```pipenv shell```

```bash

pipenv shell
Loading .env environment variables...
Launching subshell in virtual environment…
 . /home/username/.local/share/virtualenvs/pop888.pw-tqjWZUX9/bin/activate

```
тут будет путь  `/home/username/.local/share/virtualenvs/pop888.pw-tqjWZUX9/bin/python`

Пример рабочего крона 
```bash 
0 8 * * *  /home/username/.local/share/virtualenvs/pop888.pw-tqjWZUX9/bin/python first.py
0 19 * * *  /home/username/.local/share/virtualenvs/pop888.pw-tqjWZUX9/bin/python index.py

```


Если что-то не понятно велком в [директ](https://pop888.pw/direct/d.kuts)

#CHANGELOG
todo