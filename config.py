import os
from rocketchat.api import RocketChatAPI
from redminelib import Redmine
from dotenv import load_dotenv

load_dotenv()

redmine_username = os.getenv('REDMINE_USERNAME')
redmine_password = os.getenv('REDMINE_PASSWORD')

redmine = Redmine('https://r.goldmine.pw', username=redmine_username, password=redmine_password)

chat_username = os.getenv('PLATFORM_USERNAME')
chat_password = os.getenv('PLATFORM_PASSWORD')

api = RocketChatAPI(settings={'username': chat_username, 'password': chat_password,
                              'domain': 'https://platform.chat'})
#rooms = api.get_private_rooms()  # get_private_rooms

# users = api.get_my_info()


STATUS_IN_PROGRESS = 2
STATUS_CHECK_READY = 3
STATUS_CLOSE = 5


class TestSend:
    redmine_url = 'https://r.goldmine.pw/issues/'
    user_id = os.getenv('GROUP_REPORT')

    def send_in_progress(self, id, done):
        text = 'Задача ' + self.redmine_url + str(id) + ' завершена на ' + str(done) + '%'
        api.send_message(text, self.user_id)

    def send_close(self, id):
        text = 'Задача ' + self.redmine_url + str(id) + ' завершена'
        api.send_message(text, self.user_id)

    def send_today(self, id):
        text = 'Выполняю задачу ' + self.redmine_url + str(id)
        api.send_message(text, self.user_id)
