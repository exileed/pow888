from config import TestSend, redmine, STATUS_CLOSE, STATUS_IN_PROGRESS
import datetime

now = datetime.datetime.now()

issues = redmine.issue.filter(sort='updated_on:desc', assigned_to_id='me', status_id='*', updated_on=now.strftime('%Y-%m-%d'))

send = TestSend


for issue in issues:
    if issue.status.id == STATUS_IN_PROGRESS and issue.done_ratio > 0:
        send.send_in_progress(send, id=issue.id, done=issue.done_ratio)

    if issue.status.id == STATUS_CLOSE or issue.status.id == STATUS_IN_PROGRESS:
        send.send_close(send, id=issue.id)
